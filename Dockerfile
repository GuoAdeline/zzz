FROM   avic.io/public/openjdk:8-jdk-alpine
ADD target/zzz-1.0.war app.war 
EXPOSE 8080
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.war"]